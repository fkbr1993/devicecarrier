<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%signal}}`.
 */
class m190801_033958_create_signal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%signal}}', [
            'id' => $this->primaryKey(),
            'status' => $this->integer()->notNull(),
            'url' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%signal}}');
    }
}
