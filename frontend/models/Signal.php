<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "signal".
 *
 * @property int $id
 * @property int $status
 * @property string $url
 * @property int $created_at
 * @property int $updated_at
 */
class Signal extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_DONE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'signal';
    }

    public function getStatuses()
    {
        return [
            self::STATUS_NEW => 'new',
            self::STATUS_DONE => 'done',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'url', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'url' => 'Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
