<?php

namespace frontend\models;

use app\models\Signal;
use Yii;
use yii\base\Model;

/**
 * SignalForm is the model behind the contact form.
 */
class SignalForm extends Model
{
    public $url;
    public $verifyCode;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['url', 'url'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'url' => 'URL',
            'verifyCode' => 'Verification Code',
        ];
    }

    public function sendSignal()
    {
        $signal = new Signal();
        $signal->url = $this->url;
        $signal->status = Signal::STATUS_NEW;
        $signal->created_at = time();
        $signal->updated_at = time();
        if ($signal->save())
            return $signal;
        return false;
    }
}
