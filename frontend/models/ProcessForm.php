<?php

namespace frontend\models;

use app\models\Signal;
use Yii;
use yii\base\Model;

/**
 * SignalForm is the model behind the contact form.
 */
class ProcessForm extends Model
{
    public $id;
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['id', 'safe'],
            // verifyCode needs to be entered correctly
            ['file', 'file', 'extensions'=>'zip'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'file' => 'File',
        ];
    }
}
