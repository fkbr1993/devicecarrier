<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

    </div>

    <div class="body-content text-center">

        <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'signal-form']); ?>

        <?= $form->field($model, 'url')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>

        <div class="form-group">
            <?= \yii\helpers\Html::submitButton('Get screens', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>
</div>
