<?php
namespace frontend\controllers;

use app\models\Signal;
use frontend\models\ProcessForm;
use frontend\models\SignalForm;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Api controller
 */
class ApiController extends \yii\rest\Controller
{
    public function actionGetSignal()
    {
        return [Signal::find()->where(['status' => Signal::STATUS_NEW])->orderBy('id asc')->one()];
    }

    public function actionProcess()
    {
        $result = false;
        $message = '';
        $model = new ProcessForm();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post(), '');
            $model->file = UploadedFile::getInstanceByName('file');

            if ($model->validate()) {
                $signal = Signal::findOne(['id' => $model->id, 'status' => Signal::STATUS_NEW]);

                if ($signal){
                    if (!file_exists('uploads/' . $model->id . '/')){
                        mkdir('uploads/' . $model->id . '/');
                    }
                    $result = $model->file->saveAs(
                        'uploads/' . $model->id . '/' . $model->file->baseName . '.' . $model->file->extension
                    );
                }
                if ($result){
                    if ($signal){
                        $signal->status = Signal::STATUS_DONE;
                        if (!$signal->save()){
                            $result = false;
                            $message = 'model not saved';
                        }
                    }
                } else {
                    $message = 'file not saved';
                }
            }
        }

        return [
            'model' => $model,
            'result' => $result,
            'message' => $message,
        ];

    }
}
