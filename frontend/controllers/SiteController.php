<?php
namespace frontend\controllers;

use app\models\Signal;
use frontend\models\ProcessForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\SignalForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new SignalForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($signal = $model->sendSignal()) {
                Yii::$app->session->setFlash('success', 'Success');
            } else {
                Yii::$app->session->setFlash('error', 'Error');
            }
            return $this->refresh();
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpload()
    {
        $result = false;
        $model = new ProcessForm();

        if (Yii::$app->request->isPost) {
            $model->setAttributes($_POST[$model->formName()]);
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                if (!file_exists('uploads/' . $model->id . '/')){
                    mkdir('uploads/' . $model->id . '/');
                }

                $signal = Signal::findOne(['id' => $model->id, 'status' => Signal::STATUS_NEW]);
                if ($signal){
                    $result = $model->file->saveAs(
                        'uploads/' . $model->id . '/' . $model->file->baseName . '.' . $model->file->extension
                    );
                }
                if ($result){
                    if ($signal){
                        $signal->status = Signal::STATUS_DONE;
                        if (!$signal->save()){
                            $result = false;
                        }
                    }
                }
                if ($result) {
                    Yii::$app->session->setFlash('success', 'Success');
                } else {
                    Yii::$app->session->setFlash('error', 'Error');
                }
            }
        }

        return $this->render('upload', ['model' => $model, ]);
    }

    public function actionView($id)
    {
        $imageList = [];
        $dir = 'uploads/' . $id . '/';
        if (file_exists($dir)){
            $list = scandir($dir);
            foreach ($list as $file){
                if (array_reverse(explode('.', $file))[0] == 'zip'){
                    $zip = new \ZipArchive();
                    $res = $zip->open($dir . $file);
                    if ($res === TRUE) {
                        $zip->extractTo($dir);
                        $zip->close();
                        unlink($dir.$file);
                        break;
                    }
                }
            }
            $list = scandir($dir);
            foreach ($list as $file){
                if (array_reverse(explode('.', $file))[0] == 'jpeg'){
                    $imageList[] = '/' . $dir . $file;
                }
            }
        } else {
            Yii::$app->session->setFlash('error', 'Error: no files with such id');
        }

        return $this->render('view', ['images' => $imageList]);
    }
}
